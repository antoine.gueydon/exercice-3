#/bin/sh

JAVA_PATH="/c/Program Files/Java/jdk-20/bin/java"
PROJECT_PATH=/c/Users/halog/exercice-3

cp $PROJECT_PATH/petite_image_2.png ./

"$JAVA_PATH" -ea -classpath "out/production/NFA035;lib/commons-codec-1.15.jar;lib/log4j-api-2.21.0.jar;lib/org.osgi.core-6.0.0.jar" -Duser.dir=$PROJECT_PATH fr.cnam.foad.nfa035.fileutils.streaming.test.StreamingTest
rm petite_image_2.png